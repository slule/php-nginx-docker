#!/bin/sh

FILE_URL=$1

BASE_NAME=`basename $FILE_URL`

DIR_URL=`dirname $FILE_URL`

if [ ! -d .$DIR_URL ]; then
  echo "mkdir -p .$DIR_URL"
  mkdir -p .$DIR_URL
fi

if [ ! -e .$FILE_URL ]; then
  echo "cp -L $FILE_URL .$DIR_URL"
  cp -L $FILE_URL .$DIR_URL
fi