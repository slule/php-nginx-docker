#!/bin/sh

apk add --no-cache nginx-mod-http-lua

#然后就是开始打包
chmod +x /src/get_so.sh
cd /
mkdir nginx-1.21.0
cd nginx-1.21.0
ldd /usr/sbin/nginx | awk '/=>/{print $(NF-1)}' | sort -u | xargs -I {} /src/get_so.sh {}

#拷贝相关路径文件
mkdir -p ./etc/nginx
mkdir -p ./usr/sbin
mkdir -p ./usr/lib/nginx
mkdir -p ./var/log/nginx
mkdir -p ./var/run
mkdir -p ./var/cache/nginx

cp -r /etc/nginx ./etc
cp -r /usr/sbin/nginx ./usr/sbin
cp -r /usr/lib/nginx ./usr/lib
cp -r /var/log/nginx ./var/log
cp -r /var/cache/nginx ./var/cache

#打包
cd /
tar -zcvf nginx-1.21.0.tar.gz nginx-1.21.0