#!/bin/sh

#替换源
sed -i "s/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g" /etc/apk/repositories

#安装扩展依赖包
apk add --no-cache autoconf g++ libtool make curl-dev libxml2-dev linux-headers

#获取php源码
docker-php-source extract

#安装无依赖包扩展
docker-php-ext-install -j4 bcmath
docker-php-ext-install -j4 zip
docker-php-ext-install -j4 calendar
docker-php-ext-install -j4 exif
docker-php-ext-install -j4 pcntl
docker-php-ext-install -j4 pdo_mysql
docker-php-ext-install -j4 mysqli
docker-php-ext-install -j4 soap

#安装mcrypt扩展
apk add --no-cache libmcrypt-dev
docker-php-ext-install mcrypt

#安装gd扩展
apk add --no-cache freetype-dev libjpeg-turbo-dev libpng-dev
docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/
docker-php-ext-install -j4 gd

#安装gettext扩展
apk add --no-cache gettext-dev
docker-php-ext-install -j4 gettext

#安装ldap扩展
apk add --no-cache ldb-dev
apk add --no-cache openldap-dev
docker-php-ext-install -j4 ldap

#安装intl扩展
apk add --no-cache icu-dev
docker-php-ext-install -j4 intl

#安装其他扩展
apk add --no-cache wget

#安装librdkafka
wget -c https://gitee.com/tkxiong/librdkafka/repository/archive/master.zip
unzip master.zip
cd librdkafka && ./configure && make -j4 && make install

#下载其他扩展包、解压并安装
cd /usr/src/php/ext
mkdir -p protobuf grpc redis mongodb swoole rdkafka
wget -c https://pecl.php.net/get/redis-5.3.4.tgz
wget -c https://pecl.php.net/get/mongodb-1.9.1.tgz
wget -c https://pecl.php.net/get/swoole-4.4.26.tgz
wget -c https://pecl.php.net/get/rdkafka-5.0.0.tgz
wget -c https://pecl.php.net/get/protobuf-3.17.3.tgz
tar -xvf redis-5.3.4.tgz -C /usr/src/php/ext/redis/ --strip-components 1
tar -xvf mongodb-1.9.1.tgz -C /usr/src/php/ext/mongodb/ --strip-components 1
tar -xvf swoole-4.4.26.tgz -C /usr/src/php/ext/swoole/ --strip-components 1
tar -xvf rdkafka-5.0.0.tgz -C /usr/src/php/ext/rdkafka/ --strip-components 1
tar -xvf protobuf-3.17.3.tgz -C /usr/src/php/ext/protobuf/ --strip-components 1
docker-php-ext-install -j4 redis
docker-php-ext-install -j4 mongodb
docker-php-ext-install -j4 swoole
docker-php-ext-install -j4 rdkafka
docker-php-ext-install -j4 protobuf

#安装grpc，grpc依赖protobuf（这个如果项目不需要，就不要安装，安装后so文件都有80M）
#wget -c https://pecl.php.net/get/grpc-1.38.0.tgz
#tar -xvf grpc-1.38.0.tgz -C /usr/src/php/ext/grpc/ --strip-components 1
#docker-php-ext-install -j4 grpc

docker-php-source delete

#然后就是将php编译好的二进制文件打包
cd /
mkdir php-7.1.5
chmod +x /src/get_so.sh
cd php-7.1.5
ldd /usr/local/bin/php | awk '/=>/{print $(NF-1)}' | sort -u | xargs -I {} /src/get_so.sh {}
ldd /usr/local/sbin/php-fpm | awk '/=>/{print $(NF-1)}' | sort -u | xargs -I {} /src/get_so.sh {}
cp -rf /usr/local/ ./usr/local/

#拷贝其他的依赖
cp -L /usr/lib/libpng16.so.16 ./usr/lib/    #gd
cp -L /usr/lib/libintl.so.8 ./usr/lib/      #gettext
cp -L /usr/lib/libstdc++.so.6 ./usr/lib/    #grpc、swoole
cp -L /usr/lib/libicui18n.so.57 ./usr/lib/  #intl
cp -L /usr/lib/libldap-2.4.so.2 ./usr/lib/  #ldap
cp -L /usr/lib/libmcrypt.so.4 ./usr/lib/    #mcrypt
cp -L /usr/lib/libsasl2.so.3 ./usr/lib/     #mongodb、librdkafka
cp -L /usr/lib/libjpeg.so.8 ./usr/lib/      #gd.so
cp -L /usr/lib/libgcc_s.so.1 ./usr/lib/     #libstdc++.so.6
cp -L /usr/lib/libicuuc.so.57 ./usr/lib/    #intl.so
cp -L /usr/lib/liblber-2.4.so.2 ./usr/lib/  #ldap.so
cp -L /usr/lib/libltdl.so.7 ./usr/lib/      #mcrypt.so
cp -L /usr/lib/libicuuc.so.57 ./usr/lib/    #mongodb.so
cp -L /usr/lib/libgcc_s.so.1 ./usr/lib/     #swoole.so
cp -L /usr/lib/libfreetype.so.6 ./usr/lib/  #gd.so
cp -L /usr/lib/libicudata.so.57 ./usr/lib/  #intl.so
cp -L /usr/lib/libicudata.so.57 ./usr/lib/  #mongodb.so
cp -L /usr/lib/libicuio.so.57   ./usr/lib/  #intl.so

#打包
cd /
tar -zcvf php-7.1.5.tar.gz php-7.1.5